<?php

namespace App\Models;


use App\Db;
use App\Model;

class Article extends Model
{
    protected static $table = 'articles';

    public $title;
    public $description;
    public $author;
    public $date;

    public static function getLastArticles()
    {
        $db = new Db();
        $sql = 'SELECT * FROM ' . static::$table . ' ORDER BY date DESC LIMIT 3';
        return  $db->query($sql, [], static::class);
    }

}