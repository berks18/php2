<?php

$title = 'Index page';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?></title>
</head>
<body>

<h1>Articles</h1>

<?php foreach ($this->data['lastArticles'] as $article) { ?>

    <h3><?php echo $article->title; ?></h3>
    <p><?php echo mb_strimwidth($article->description, 0, 250, "..."); ?></p>
    <p><?php echo $article->author; ?></p>
    <p><?php echo date('d-m-Y', $article->date); ?></p>
    <p><a href="/article.php?id=<?php echo $article->id; ?>">Read...</a></p>

<?php } ?>

</body>
</html>


