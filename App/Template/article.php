<?php

$article = $this->data['article'][0];
$title = $article->title;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?></title>
</head>
<body>

    <h1><?php echo $article->title; ?></h1>

    <?php foreach (explode(PHP_EOL, $article->description) as $paragraph) { ?>
        <p><?php echo $paragraph; ?></p>
    <?php } ?>

    <p><?php echo $article->author; ?></p>
    <p><?php echo date('d-m-Y', $article->date); ?></p>
    <p><a href="/">Return home...</a></p>

</body>
</html>


