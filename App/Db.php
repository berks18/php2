<?php

namespace App;


class Db
{
    protected $dbh;

    public function __construct()
    {
        $dsn = 'mysql:host=localhost;dbname=oop';
        $this->dbh = new \PDO ($dsn, 'root', '');
    }

    public function query($sql, $data = [], $class = '')
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($data);
        return $sth->fetchAll(\PDO::FETCH_CLASS, $class);;
    }
}