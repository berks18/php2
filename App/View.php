<?php

namespace App;


class View
{
    protected $data = [];

    public function assign($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function display($template)
    {
        include __DIR__ . './Template/' . $template . '.php';
    }

    public function render($template)
    {
        ob_start();
        include __DIR__ . './Template/' . $template . '.php';
        $out_page = ob_get_contents();
        ob_get_clean();
        return $out_page;
    }
}