<?php

namespace App;


abstract class Model
{
    protected static $table = '';

    public $id;

    public static function findById($id)
    {
        $db = new Db;
        $sql = 'SELECT * FROM ' . static::$table . ' WHERE id=:id';
        $query = $db->query($sql, [':id' => (int)$id], static::class);
        return  ( empty($query) ) ? false : $query;
    }

    public static function findAll()
    {
        $db = new Db;
        $sql = 'SELECT * FROM ' . static::$table;
        return  $db->query($sql, [], static::class);
    }
}