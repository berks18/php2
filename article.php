<?php

use App\Models\Article;
use App\View;

require __DIR__ . '/autoload.php';

if( isset($_GET['id']) ) {
    $id = $_GET['id'];
    $article = Article::findById($id);

    $view = new View();
    $view->assign('article', $article);
    $view->display('article');
} else {
    $host  = $_SERVER['HTTP_HOST'];
    header('Location: http://' . $host );
}