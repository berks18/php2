<?php

use App\Models\Article;
use App\View;

require __DIR__ . '/autoload.php';

$lastArticles = Article::getLastArticles();

$view = new View();
$view->assign('lastArticles', $lastArticles);
$view->display('index');