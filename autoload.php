<?php
/**
 * Created by PhpStorm.
 * User: Lysak Tatiana
 * Date: 25.11.2018
 * Time: 12:25
 */

function __autoload($class)
{
    $file = __DIR__ . '/' . str_replace('\\', '/', $class ) . '.php';
    require $file;
}